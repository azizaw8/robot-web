# Importation des bibliothèques nécessaires
import requests
import re
from bs4 import BeautifulSoup
import telegram
import asyncio
 #traitement1:
# Définir l'URL de la page à scraper
url = 'http://pagesjaunesdusenegal.com/pages/resultat.php?quoi=medecin'

# Envoyer une requête pour récupérer le contenu de l:;a page
page = requests.get(url)

# Utiliser BeautifulSoup pour extraire les adresses e-mail
soup = BeautifulSoup(page.content, 'html.parser')
emails = [mail.get('href') for mail in soup.find_all('a') if mail.get('href') and mail.get('href').startswith('mailto:')]

# Extraire les adresses e-mail à l'aide d'expressions régulières
emails = re.findall(r'[\w\.-]+@[\w\.-]+', page.text)

# Afficher les adresses e-mail extraites 
print(emails)

#traitement 2
# Se connecter à Telegram
bot = telegram.Bot(token='6179234921:AAEcrR7AEWQunNmfAuITAY5tPEkKEJh1dNs')

# Définir une fonction pour envoyer un e-mail
async def send_email(email):
    await bot.send_message(chat_id=-1001864930281, text=email)

# Définir la fonction principale qui exécutera la boucle d'événements
async def main():
    # Pour chaque e-mail, envoyer un message à Telegram
    for email in emails:
        await send_email(email)

# Créer la boucle d'événements
loop = asyncio.get_event_loop()

# Exécuter la boucle d'événements jusqu'à ce que toutes les tâches soient terminées
loop.run_until_complete(main())

# Fermer la boucle d'événements
loop.close()


