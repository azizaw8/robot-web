import unittest
import re
from telegram import Bot, User

class TestEmailExtraction(unittest.TestCase):
    
    def test_email_extraction(self):
        # Créer une chaîne de caractères avec des adresses e-mail
        test_string = 'aziz@gmail.com, ndiouga@gmail.com, test.email@example.com'
        
        # Vérifier que la fonction extrait correctement toutes les adresses e-mail de la chaîne de caractères
        expected_result = ['aziz@gmail.com', 'ndiouga@gmail.com', 'test.email@example.com']
        self.assertEqual(re.findall(r'[\w\.-]+@[\w\.-]+', test_string), expected_result)

import telegram
import unittest

class TestTelegramConnection(unittest.TestCase):
    
    async def test_telegram_connection(self):
        bot_token = '6179234921:AAEcrR7AEWQunNmfAuITAY5tPEkKEJh1dNs'
        
        # Connect to Telegram
        bot = telegram.Bot(token=bot_token)
        
        # Check that the connection is established
        self.assertIsInstance(await bot.get_me(), telegram.User)

if __name__ == '__main__':
    unittest.main()

